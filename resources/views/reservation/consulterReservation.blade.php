@extends('layouts.default')

@section('title')

<h1> Récapitulatif de votre demande de réservation </h1>

@endsection

@section('content')



<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">villeDepart</th>
            <th scope="col">villeArrivee</th>          
            <th scope="col">dateDebutReservation</th>
            <th scope="col">numeroDeReservation</th>
            <th scope="col">dateFinReservation</th>
            <th scope="col">etat</th>
            <th scope="col">dateReservation</th>            
            <th scope="col">volumeEstime</th>
            <th scope="col">libelleTypeContainer</th>
            <th scope="col">qteReserver</th>
        </tr>
    </thead>
    <tbody>

         @foreach($collectionReservationEtLigneDeReservation as $uneLigneDeReservation)
        <tr>
            <td>{{$uneLigneDeReservation["villeDepart"]}}</td>
            <td>{{$uneLigneDeReservation["villeArrivee"]}}</td>
            <td>{{date('Y-m-d à H:m',$uneLigneDeReservation["dateDebutReservation"])}}</td>
            <td>{{$uneLigneDeReservation["numeroDeReservation"]}}</td>
            <td>{{date('Y-m-d à H:m',$uneLigneDeReservation["dateFinReservation"])}}</td>
            <td>{{$uneLigneDeReservation["etat"]}}</td>
            <td>{{date('Y-m-d à H:m',$uneLigneDeReservation["dateReservation"])}}</td>
            <td>{{$uneLigneDeReservation["volumeEstime"]}}</td>
            <td>{{$uneLigneDeReservation["libelleTypeContainer"]}}</td>
            <td>{{$uneLigneDeReservation["qteReserver"]}}</td>
            
        </tr>
        @endforeach
        
    </tbody>
</table>




@endsection