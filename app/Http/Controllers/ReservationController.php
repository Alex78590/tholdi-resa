<?php

namespace App\Http\Controllers;

use App\Http\Model\Data;
use DateTime;
use Illuminate\Http\Request;
use Iluminate\Support\Facades\DB;

class ReservationController extends Controller
{
    public function saisirReservation(){
        $collectionVilles = Data::obtenirCollectionVille();
        return view('reservation.saisirReservation',['collectionVilles'=>$collectionVilles]);
    }
    
    public function ajouterReservation(Request $request){
        $dateDebutReservation=$request->input('dateDebutReservation');
        $dateFinReservation=$request->input('dateFinReservation');
        $dateDebutReservation=$this->dateTimeToTimestamp($dateDebutReservation);
        $dateFinReservation=$this->dateTimeToTimestamp($dateFinReservation);
        $codeVilleMiseDispo=$request->input('codeVilleMiseDisposition');
        $codeVilleRendre=$request->input('codeVilleRendre');
        $volumeEstime=$request->input('volumeEstime');
        $compteUtilisateur=$request->session()->get('utilisateur');
        $code=$compteUtilisateur["code"];
        $codeResevation=Data::AjouterUneReservation($dateDebutReservation,$dateFinReservation,time(),$codeVilleMiseDispo,$codeVilleRendre,$volumeEstime,$code);
        session()->put('codeReservation',$codeResevation);
        $typeContainer=Data::obtenirCollectionTypeContainer();
        $view=view('reservation.saisirLigneDeReservation',['typeContainer'=>$typeContainer]);
        return $view;
    }
    
    private function dateTimeToTimestamp($date){
        $dt=new DateTime($date);
        return $dt->getTimestamp();
    }
    
    public function ajouterLigneReservation(Request $request){
        $numeroTypeContainer=$request->input('container');
        $qteReserver=$request->input('quantite');
        if ($request->session()->has('LesLignesDeReservation')!=true){
            $collectionLignesReservation=collect();
        }
        else{
            $collectionLignesReservation=$request->session()->get('LesLignesDeReservation');
        }
        $collectionLignesReservation->push(['numTypeContainer'=>$numeroTypeContainer,'qteReserver'=>$qteReserver]);
        $request->session()->put('LesLignesDeReservation',$collectionLignesReservation);
        $typeContainer=Data::obtenirCollectionTypeContainer();
        $collectionTypeContainer=collect($typeContainer);
        $collectionTypeContainer->mergeByDesiredKey($collectionLignesReservation,"numTypeContainer");
        return view ('reservation.saisirLigneDeReservation',['typeContainer'=>$collectionTypeContainer,]);
    }
    
    public function finaliserLaReservation(Request $request){
        $codeReservation=$request->session()->get('codeReservation');
        $lesLignesDeReservation=$request->session()->get('LesLignesDeReservation');
        if ($lesLignesDeReservation != null){
            foreach ($lesLignesDeReservation as $uneLigneDeReservation){
                $numTypeContainer=$uneLigneDeReservation["numTypeContainer"];
                $qteReserver=$uneLigneDeReservation["qteReserver"];
                Data::ajouterUneLigneDeReservation($codeReservation,$numTypeContainer,$qteReserver);
            }
            $request->session()->remove('LesLignesDeReservation');
            $ligneReservation=Data::obtenirCollectionLignesDeUneReservation($codeReservation);
            $codeOrder=$request->session()->get('codeReservation');
            $request->session()->put('codeOrder',$codeOrder);
            $request->session()->remove('codeReservation');
            $view=view('reservation.recapitulatifDemandeDeReservation',['CollectionlignesReservation'=>$ligneReservation]);
        }
        else {
            $view=$this->consulterReservation($request);
        }
        return $view;
    }
    
     //public function consulterReservation(Request $request){
     //   $compteUtilisateur=$request->session()->get('utilisateur');
     //   $code=$compteUtilisateur["code"];
     //   $collectionReservationEtLigneDeReservation=Data::obtenirCollectionReservationEtLigneDeReservationPourUnUtilisateur($code);
     //   return view ('reservation.consulterReservation',['collectionReservationEtLigneDeReservation'=>$collectionReservationEtLigneDeReservation]);
     //}
    
    public function consulterReservation (Request $request){
        $compteUtilisateur = $request->session()->get('utilisateur');
        $codeReservation = $compteUtilisateur["code"];
        $collectionReservationEtLigneDeReservation = DB::table('reservation')
                ->select('*','v.nomVille as villeDepart','v2.nomVille as villeArrivee') 
                ->leftJoin('utilisateur','utilisateur.code','=','reservation.codeUtilisateur')
                ->leftJoin('reserver','reserver.codeReservation','=','reservation.codeReservation')
                ->leftJoin('typeContainer','reserver.numTypeContainer','=','typeContainer.numTypeContainer')
                ->leftJoin('ville as v','v.codeVille','=','reservation.codeVilleMiseDisposition')
                ->leftJoin('ville as v2','v2.codeVille','=','reservation.codeVilleRendre')
                ->where('utilisateur.code','=',$codeReservation)
                ->get();
        $reservations = $collectionReservationEtLigneDeReservation->groupBy('codeReservation');
        $reservations = $reservations->toArray();
        return view('reservation.consulterReservation',['collectionReservationEtLigneDeReservation'=>$reservations]);
    }
    
    
    
    public function orderQte(Request $request,$choix){
        switch ($choix){
            case "quantite":
        
        $codeOrder=$request->session()->get('codeOrder');
        $ligneOrder=Data::obtenirOrderQte($codeOrder);
        $view=view('reservation.recapitulatifDemandeDeReservation',['CollectionlignesReservation'=>$ligneOrder]);
        return $view;
        break;
    
            case "container":
        $codeOrder=$request->session()->get('codeOrder');
        $ligneOrder=Data::obtenirOrderContainer($codeOrder);
        $view=view('reservation.recapitulatifDemandeDeReservation',['CollectionlignesReservation'=>$ligneOrder]);
        return $view;
        break;
    }
  }
  
}
