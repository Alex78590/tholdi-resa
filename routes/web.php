<?php
Route::post('AuthentificationCompteUtilisateur',
        'AuthentificationController@authentificationCompteUtilisateur')
        ->name('AuthentificationCompteUtilisateur');

Route::view('/','accueil')->name('PageAccueil');
Route::group(['prefix'=>'reservation'], function(){Route::get('SaisirReservation','ReservationController@SaisirReservation')->name('SaisirReservation'); 
Route::post('AjouterReservation','ReservationController@ajouterReservation')->name('AjouterReservation');
Route::post('AjouterLigneReservation','ReservationController@ajouterLigneReservation')->name('AjouterLigneReservation');
Route::post('FinaliserLaReservation','ReservationController@finaliserLaReservation')->name('FinaliserLaReservation');
Route::get('OrderQte{choix}','ReservationController@orderQte')->name('OrderQte');
Route::get('ConsulterReservation','ReservationController@consulterReservation')->name('ConsulterReservation');
Route::post('AnnulerReservation','ReservationController@annulerReservation')->name('AnnulerReservation');
});